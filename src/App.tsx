import React, { useState } from "react";
import "./App.css";
import Barcode from "react-barcode";

function BottleBarcodeGenerator() {
  const [smallBottle, setSmallBottle] = useState(true);
  const [lotNr, setLotLr] = useState(11);

  let digitsToSum: number[] = [];

  //Initial Bar Code Digits
  let bcd = [5, 5, 2, 6, 1, 1, 2, 8];

  // Four first digits of the barcode are the product code.
  // Here hardcoded to 5526 for the OKP ID
  const productCode = 5526;

  let getSumOfDigitsAndFactors = () => {
    let factors = [3, 1, 3, 1, 3, 1, 3];
    let sum = 0;
    for (let i = 0; i < factors.length; i++) {
      digitsToSum[i] = bcd[i] * factors[i];
      sum += digitsToSum[i];
    }
    return sum;
  };

  let getCheckDigit = () => {
    let sum = getSumOfDigitsAndFactors();
    let step1 = sum / 10;
    let step2 = Math.ceil(step1);
    let step3 = step2 * 10;
    let step4 = step3 - sum;
    return step4;
  };

  const onChangeHandler = (event: any) => {
    setLotLr(event.target.value);
  };

  let getBarcodeDigits = () => {
    let okp = productCode.toString();
    let lot = lotNr.toString();

    bcd[0] = parseInt(okp.charAt(0));
    bcd[1] = parseInt(okp.charAt(1));
    bcd[2] = parseInt(okp.charAt(2));
    bcd[3] = parseInt(okp.charAt(3));
    bcd[4] = parseInt(lot.charAt(0));
    bcd[5] = parseInt(lot.charAt(1)) || 0;
    bcd[6] = smallBottle ? 2 : 3;
    bcd[7] = getCheckDigit();
    return bcd.join("");
  };

  return (
    <div className="App">
      <header></header>
      <body>
        <h1>Strekkoder til reagensglass</h1>
        <div className="formElement">
          <label>Produktkode / OKP ID </label>
          <input type="number" readOnly={true} value={productCode}></input>
        </div>

        <div className="formElement">
          <label>To siste siffer i lotnummeret fra Medirox</label>
          <input
            type="number"
            max={9999}
            onChange={onChangeHandler}
            value={lotNr}
          ></input>
        </div>

        <div className="formElement">
          <label>Flaskestørrelse. Klikk for å endre </label>
          <button
            type="button"
            onClick={() => setSmallBottle((smallBottle) => !smallBottle)}
          >
            valgt : {smallBottle ? "GW 5" : "GW 15"}
          </button>
        </div>

        <div className="formElement">
          <Barcode
            className="barcode"
            value={`${getBarcodeDigits()}`}
            height={50}
            width={3}
            fontSize={30}
          />
        </div>
      </body>
      <footer>
        <p>
          Laget av <a href="http://fredriklindseth.no">Fredrik</a>. Kildekode på{" "}
          <a href="https://gitlab.com/FredrikLindseth/strekkodegenerator">
            GitLab
          </a>
        </p>
      </footer>
    </div>
  );
}

export default BottleBarcodeGenerator;
function setValue(value: string): void {
  throw new Error("Function not implemented.");
}
