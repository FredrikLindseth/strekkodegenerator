# Strekkodegenerator

Dings for å lage strekkoder til reagensglass til siemens sine maskiner

Tilgjengelig på [0v.no/strekkode](https://0v.no/strekkode)

![illustrasjonsbilde](screenshot.png)

## Oppsett

```sh
git clone git@gitlab.com:FredrikLindseth/strekkodegenerator.git
cd strekkodegenerator
npm i
npm run dev
```

## Bygg og deploy

```sh
run run build
scp -r /home/fredrik/workspace/strekkodegenerator/dist fredrik@0v.no:/home/fredrik/strekkode
```
